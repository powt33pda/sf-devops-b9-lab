# Install tools
sudo apt-get update
sudo apt install -y curl ca-certificates git

# Install Docker
sudo apt install -y docker docker.io
sudo apt install -y docker-compose

# Install Postgres
sudo apt update
sudo apt install postgresql postgresql-contrib

# Setup Postgres
sudo -u postgres createuser -s sf_devops
sudo -u postgres createdb sf_devops
sudo -u postgres psql -c "ALTER USER sf_devops PASSWORD 'pass1234';" # Not secure, but fine for education

# Pull sources
mkdir src
git clone https://github.com/SkillfactoryCoding/DEVOPS-praktikum_Docker.git src
